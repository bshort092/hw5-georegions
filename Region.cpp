//
// Created by Stephen Clyde on 3/4/17.
//

#include "Region.h"
#include "Utils.h"
#include "World.h"
#include "Nation.h"
#include "State.h"
#include "County.h"
#include "City.h"

#include <iomanip>
#include <string.h>

const std::string regionDelimiter = "^^^";
const int TAB_SIZE = 4;
unsigned int Region::m_nextId = 0;

Region* Region::create(std::istream &in)
{
    Region* region = nullptr;
    std::string line;
    std::getline(in, line);
    if (line!="")
    {
        region = create(line);
        if (region!= nullptr)
            region->loadChildren(in);
    }
    return region;
}
Region* Region::create(const std::string& data)
{
    Region* region = nullptr;
    std::string regionData;
    unsigned long commaPos = (int) data.find(",");
    if (commaPos != std::string::npos)
    {
        std::string regionTypeStr = data.substr(0,commaPos);
        regionData = data.substr(commaPos+1);

        bool isValid;
        RegionType regionType = (RegionType) convertStringToInt(regionTypeStr, &isValid);

        if (isValid)
        {
            region = create(regionType, regionData);
        }

    }

    return region;
}

Region* Region::create(RegionType regionType, const std::string& data)
{
    Region* region = nullptr;
    std::string fields[3];
    if (split(data, ',', fields, 3)) {

        // Create the region based on type
        switch (regionType) {
            case WorldType:
                region = new World();
                break;
            case NationType:
                region = new Nation(fields);
                break;
       //Add cases for State, County, and City
            case StateType:
                region = new State(fields);
                break;
            case CountyType :
                region = new County(fields);
                break;
            case CityType:
                region = new City(fields);
                break;
            default:
                break;
        }

        // If the region isn't valid, get rid of it
        if (region != nullptr && !region->getIsValid()) {
            delete region;
            region = nullptr;
        }
    }

    return region;
}


std::string Region::regionLabel(RegionType regionType)
{
    std::string label = "Unknown";
    switch (regionType)
    {
        case Region::WorldType:
            label = "World";
            break;
        case Region::NationType:
            label = "Nation";
            break;
        case Region::StateType:
            label = "State";
            break;
        case Region::CountyType:
            label = "County";
            break;
        case Region::CityType:
            label = "City";
            break;
        default:
            break;
    }
    return label;
}

Region::Region() { }

Region::Region(RegionType type, const std::string data[]) :
        m_id(getNextId()), m_regionType(type), m_isValid(true)
{
    m_name = data[0];
    m_population = convertStringToUnsignedInt(data[1], &m_isValid);
    if (m_isValid)
        m_area = convertStringToDouble(data[2], &m_isValid);
}

Region::~Region()
{
    if (m_sizeOfArray != 0)
    {
        for (int i = 0; i < m_numberSubRegions; i++) {
            delete m_subRegions[i];
        }
        delete [] m_subRegions;
    }
}

std::string Region::getRegionLabel() const
{
    return regionLabel(getType());
}

unsigned int Region::computeTotalPopulation()
{
    // TODO: implement computeTotalPopulation, such that the result is m_population + the total population for all sub-regions
    if(getType() == Region::CityType)
    {
        return m_population;
    }
    unsigned int totalPopulation = m_population;

    for (int i = 0; i <m_numberSubRegions ; i++)
    {
        if(m_subRegions[i]!= nullptr && m_subRegions[i]->getIsValid())
        {
            totalPopulation += m_subRegions[i]->computeTotalPopulation();
        }
    }
    return totalPopulation;
}

void Region::list(std::ostream& out)
{
    out << std::endl;
    out << getName() << ":" << std::endl;

    // TODO: implement the loop in the list method
    // for each subregion, print out
    //      id    name
    for (int i = 0; i < m_numberSubRegions; i++) {
        if(m_subRegions != nullptr && m_subRegions[i]->getIsValid()) {
            out << m_subRegions[i]->getId() << m_subRegions[i]->getName() << endl;
        }
    }
}

void Region::display(std::ostream& out, unsigned int displayLevel, bool showChild)
{
    if (displayLevel>0)
    {
        out << std::setw(displayLevel * TAB_SIZE) << " ";
    }

    unsigned totalPopulation = computeTotalPopulation();
    double area = getArea();
    double density = (double) totalPopulation / area;

    // TODO: compute the totalPopulation using a method

    out << std::setw(6) << getId() << "  "
        << getName() << ", population="
        << totalPopulation
        << ", area=" << area
        << ", density=" << density << std::endl;

    if (showChild)
    {
        // TODO: implement loop in display method
        // foreach subregion
        //      display that subregion at displayLevel+1 with the same showChild value
        for (int i = 0; i <m_numberSubRegions ; i++)
        {
            if(m_subRegions != nullptr && m_subRegions[i]->getIsValid())
            {
                m_subRegions[i]->display(out, displayLevel+1, showChild);
            }
        }
    }
}

void Region::save(std::ostream& out)
{
    out << getType()
        << "," << getName()
        << "," << getPopulation()
        << "," << getArea()
        << std::endl;

    // TODO: implement loop in save method to save each sub-region
    // foreach subregion,
    //      save that region
    for (int i = 0; i <m_numberSubRegions ; i++) {
        if(m_subRegions != nullptr && m_subRegions[i]->getIsValid()) {
            m_subRegions[i]->save(out);
        }
    }
    out << regionDelimiter << std::endl;
}

void Region::validate()
{
    m_isValid = (m_area!=UnknownRegionType && m_name!="" && m_area>=0);
}

void Region::loadChildren(std::istream& in) {
    std::string line;
    bool done = false;
    while (!in.eof() && !done) {
        std::getline(in, line);
        if (line == regionDelimiter) {
            done = true;
        } else {
            Region *child = create(line);
            if (child != nullptr) {
                // TODO: Add the new sub-region to this region
                addRegion(child);
                child->loadChildren(in);
            }
            else {
                done = true;
            }
        }
    }
}
unsigned int Region::getNextId()
{
    if (m_nextId==UINT32_MAX)
        m_nextId=1;

    return m_nextId++;
}
//My functions
unsigned int Region::getSubRegionCount() {
    return m_numberSubRegions;
}

Region* Region::getSubRegionByIndex(int index) {
    Region* result = nullptr;
    if (m_numberSubRegions>0 && index < m_numberSubRegions)
    {
        result = m_subRegions[index];
    }
    return result;
}
void Region::addRegion(Region* region) {


    if (m_sizeOfArray == 0) {
        m_subRegions = new Region *[10];
        m_sizeOfArray =10;
    }

    if (m_numberSubRegions == m_sizeOfArray)
        grow();

    m_subRegions[m_numberSubRegions++] = region;
}

void Region::grow(){

    m_sizeOfArray *= 2;
    Region** newRegions = new Region*[m_sizeOfArray];

    for (int i=0; i<m_sizeOfArray; i++)
    {
        newRegions[i] = m_subRegions[i];
    }

    delete [] m_subRegions;

    m_subRegions = newRegions;
}
Region* Region::getSubRegionByIdReturnsPointer(int id)
{
    Region* region = nullptr;
    for (int i = 0; i <m_numberSubRegions ; i++)
    {
        if(id == m_subRegions[i]->getId())
        {
            region = m_subRegions[i];
        }
    }
    return region;
}
int Region::getSubRegionById(int id){
    int result = -1;
    for (int i = 0; i <m_numberSubRegions ; i++)
    {
        if(id == m_subRegions[i]->getId())
        {
            result = i;
        }
    }
    return result;
}
void Region::removeSubRegion(int index)
{
    if(index < m_numberSubRegions)
    {
        delete m_subRegions[index];
        for (int i = index; i <m_numberSubRegions -1; i++)
        {
            m_subRegions[index] = m_subRegions[index +1];
        }
        m_numberSubRegions--;
    }
}